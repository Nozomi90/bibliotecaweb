package it.cloudtec.biblioteca.portlet.portlet;

import it.cloudtec.biblioteca.constants.BibliotecaWebPortletKeys;
import it.cloudtec.biblioteca.service.AutoreLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Cristian
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=BibliotecaWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + BibliotecaWebPortletKeys.BIBLIOTECAWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class BibliotecaWebPortlet extends MVCPortlet {
	
	@Reference
	private AutoreLocalService autoreLocalService;
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		renderRequest.setAttribute(AutoreLocalService.class.getName(), autoreLocalService);
		
		
		super.doView(renderRequest, renderResponse);
	}
	
}