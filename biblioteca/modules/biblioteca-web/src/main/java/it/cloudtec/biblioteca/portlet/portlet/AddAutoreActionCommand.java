package it.cloudtec.biblioteca.portlet.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import it.cloudtec.biblioteca.constants.BibliotecaWebPortletKeys;
import it.cloudtec.biblioteca.model.Autore;
import it.cloudtec.biblioteca.service.AutoreLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name="+BibliotecaWebPortletKeys.BIBLIOTECAWEB,
				"mvc.command.name=addAutore"
		},
		service = MVCActionCommand.class
		)
public class AddAutoreActionCommand implements MVCActionCommand {

	@Reference
	private AutoreLocalService autoreLocalService;

	@Override
	public boolean processAction(
			ActionRequest actionRequest, ActionResponse actionResponse)
					throws PortletException {

		try {
			_handleActionCommand(actionRequest);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return true;
	}

	private void _handleActionCommand(ActionRequest actionRequest) throws PortalException {

		String nome = ParamUtil.getString(actionRequest,"nome");
		String categoria = ParamUtil.getString(actionRequest,"categoria");
		long companyId = PortalUtil.getCompanyId(actionRequest);
		long groupId = PortalUtil.getScopeGroupId(actionRequest);
		long userId = PortalUtil.getUserId(actionRequest);
		
		

		Autore nuovoAutore = autoreLocalService.nuovoAutore(companyId, groupId, userId, nome, categoria,  ServiceContextFactory.getInstance(actionRequest));
		


		actionRequest.setAttribute("nuovoAutore", nuovoAutore);	

	}
}