<%@ include file="/init.jsp" %>

<%
List<Autore> listaAutori = autoreLocalService.getAutores(QueryUtil.ALL_POS, QueryUtil.ALL_POS);

%>

<liferay-portlet:actionURL name="addAutore" var="addAutoreURL"></liferay-portlet:actionURL>

<aui:form action="<%= addAutoreURL %>" method="post" name="fm" id="myform">

	<aui:input name="nome" type="text" />
	<aui:input name="categoria" type="text" />

	<aui:button-row>
		<aui:button value="Inserisci" type="submit"></aui:button>
	</aui:button-row>

</aui:form>


<table class="table table-striped">

	<thead>

		<tr>
			
			<th scope="col">Nome</th>
			<th scope="col">Numero Opere</th>
			<th scope="col">Categoria</th>
		</tr>

	</thead>
	
	<tbody>

		<%
		
			for( Autore item: listaAutori) {
		%>
		
		<tr>
					<td><%=item.getNome() %></td>
					<td><%=item.getNumeroOpere() %></td>
					<td><%=item.getCategoria() %></td>					
				</tr>
				
		<%
			}
		%>
		
	</tbody>	
	
</table>	


