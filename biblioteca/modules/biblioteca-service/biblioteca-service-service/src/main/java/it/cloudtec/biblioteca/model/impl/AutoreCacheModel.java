/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.cloudtec.biblioteca.model.Autore;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Autore in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class AutoreCacheModel implements CacheModel<Autore>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AutoreCacheModel)) {
			return false;
		}

		AutoreCacheModel autoreCacheModel = (AutoreCacheModel)obj;

		if (autoreId == autoreCacheModel.autoreId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, autoreId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", autoreId=");
		sb.append(autoreId);
		sb.append(", nome=");
		sb.append(nome);
		sb.append(", numeroOpere=");
		sb.append(numeroOpere);
		sb.append(", categoria=");
		sb.append(categoria);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Autore toEntityModel() {
		AutoreImpl autoreImpl = new AutoreImpl();

		if (uuid == null) {
			autoreImpl.setUuid("");
		}
		else {
			autoreImpl.setUuid(uuid);
		}

		autoreImpl.setStatus(status);
		autoreImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			autoreImpl.setStatusByUserName("");
		}
		else {
			autoreImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			autoreImpl.setStatusDate(null);
		}
		else {
			autoreImpl.setStatusDate(new Date(statusDate));
		}

		autoreImpl.setGroupId(groupId);
		autoreImpl.setCompanyId(companyId);
		autoreImpl.setUserId(userId);

		if (userName == null) {
			autoreImpl.setUserName("");
		}
		else {
			autoreImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			autoreImpl.setCreateDate(null);
		}
		else {
			autoreImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			autoreImpl.setModifiedDate(null);
		}
		else {
			autoreImpl.setModifiedDate(new Date(modifiedDate));
		}

		autoreImpl.setAutoreId(autoreId);

		if (nome == null) {
			autoreImpl.setNome("");
		}
		else {
			autoreImpl.setNome(nome);
		}

		autoreImpl.setNumeroOpere(numeroOpere);

		if (categoria == null) {
			autoreImpl.setCategoria("");
		}
		else {
			autoreImpl.setCategoria(categoria);
		}

		autoreImpl.resetOriginalValues();

		return autoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		autoreId = objectInput.readLong();
		nome = objectInput.readUTF();

		numeroOpere = objectInput.readLong();
		categoria = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(autoreId);

		if (nome == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nome);
		}

		objectOutput.writeLong(numeroOpere);

		if (categoria == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(categoria);
		}
	}

	public String uuid;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long autoreId;
	public String nome;
	public long numeroOpere;
	public String categoria;

}