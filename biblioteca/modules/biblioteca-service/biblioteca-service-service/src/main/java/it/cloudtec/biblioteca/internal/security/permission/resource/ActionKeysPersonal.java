package it.cloudtec.biblioteca.internal.security.permission.resource;

import com.liferay.portal.kernel.security.permission.ActionKeys;

public class ActionKeysPersonal extends ActionKeys {
	
	
	public static final String AGGIUNGI_AUTORE ="AGGIUNGI_AUTORE";
	public static final String CANCELLA_AUTORE = "CANCELLA_AUTORE";
	public static final String AGGIORNA_AUTORE = "AGGIORNA_AUTORE";
	public static final String VISUALIZZA_LISTA_AUTORI = "VISUALIZZA_LISTA_AUTORI";

}
