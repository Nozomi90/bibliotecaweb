/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.cloudtec.biblioteca.model.Opera;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Opera in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class OperaCacheModel implements CacheModel<Opera>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OperaCacheModel)) {
			return false;
		}

		OperaCacheModel operaCacheModel = (OperaCacheModel)obj;

		if (operaId == operaCacheModel.operaId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, operaId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", operaId=");
		sb.append(operaId);
		sb.append(", autoreId=");
		sb.append(autoreId);
		sb.append(", titolo=");
		sb.append(titolo);
		sb.append(", categoria=");
		sb.append(categoria);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Opera toEntityModel() {
		OperaImpl operaImpl = new OperaImpl();

		if (uuid == null) {
			operaImpl.setUuid("");
		}
		else {
			operaImpl.setUuid(uuid);
		}

		operaImpl.setStatus(status);
		operaImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			operaImpl.setStatusByUserName("");
		}
		else {
			operaImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			operaImpl.setStatusDate(null);
		}
		else {
			operaImpl.setStatusDate(new Date(statusDate));
		}

		operaImpl.setGroupId(groupId);
		operaImpl.setCompanyId(companyId);
		operaImpl.setUserId(userId);

		if (userName == null) {
			operaImpl.setUserName("");
		}
		else {
			operaImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			operaImpl.setCreateDate(null);
		}
		else {
			operaImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			operaImpl.setModifiedDate(null);
		}
		else {
			operaImpl.setModifiedDate(new Date(modifiedDate));
		}

		operaImpl.setOperaId(operaId);
		operaImpl.setAutoreId(autoreId);

		if (titolo == null) {
			operaImpl.setTitolo("");
		}
		else {
			operaImpl.setTitolo(titolo);
		}

		if (categoria == null) {
			operaImpl.setCategoria("");
		}
		else {
			operaImpl.setCategoria(categoria);
		}

		operaImpl.resetOriginalValues();

		return operaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		operaId = objectInput.readLong();

		autoreId = objectInput.readLong();
		titolo = objectInput.readUTF();
		categoria = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(operaId);

		objectOutput.writeLong(autoreId);

		if (titolo == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(titolo);
		}

		if (categoria == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(categoria);
		}
	}

	public String uuid;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long operaId;
	public long autoreId;
	public String titolo;
	public String categoria;

}