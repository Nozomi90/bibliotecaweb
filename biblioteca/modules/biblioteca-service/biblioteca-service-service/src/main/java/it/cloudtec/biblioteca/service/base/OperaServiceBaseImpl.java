/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service.base;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.service.BaseServiceImpl;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.cloudtec.biblioteca.model.Opera;
import it.cloudtec.biblioteca.service.OperaService;
import it.cloudtec.biblioteca.service.persistence.AutorePersistence;
import it.cloudtec.biblioteca.service.persistence.OperaPersistence;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the opera remote service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link it.cloudtec.biblioteca.service.impl.OperaServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.cloudtec.biblioteca.service.impl.OperaServiceImpl
 * @generated
 */
public abstract class OperaServiceBaseImpl
	extends BaseServiceImpl implements OperaService, IdentifiableOSGiService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>OperaService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>it.cloudtec.biblioteca.service.OperaServiceUtil</code>.
	 */

	/**
	 * Returns the autore local service.
	 *
	 * @return the autore local service
	 */
	public it.cloudtec.biblioteca.service.AutoreLocalService
		getAutoreLocalService() {

		return autoreLocalService;
	}

	/**
	 * Sets the autore local service.
	 *
	 * @param autoreLocalService the autore local service
	 */
	public void setAutoreLocalService(
		it.cloudtec.biblioteca.service.AutoreLocalService autoreLocalService) {

		this.autoreLocalService = autoreLocalService;
	}

	/**
	 * Returns the autore remote service.
	 *
	 * @return the autore remote service
	 */
	public it.cloudtec.biblioteca.service.AutoreService getAutoreService() {
		return autoreService;
	}

	/**
	 * Sets the autore remote service.
	 *
	 * @param autoreService the autore remote service
	 */
	public void setAutoreService(
		it.cloudtec.biblioteca.service.AutoreService autoreService) {

		this.autoreService = autoreService;
	}

	/**
	 * Returns the autore persistence.
	 *
	 * @return the autore persistence
	 */
	public AutorePersistence getAutorePersistence() {
		return autorePersistence;
	}

	/**
	 * Sets the autore persistence.
	 *
	 * @param autorePersistence the autore persistence
	 */
	public void setAutorePersistence(AutorePersistence autorePersistence) {
		this.autorePersistence = autorePersistence;
	}

	/**
	 * Returns the opera local service.
	 *
	 * @return the opera local service
	 */
	public it.cloudtec.biblioteca.service.OperaLocalService
		getOperaLocalService() {

		return operaLocalService;
	}

	/**
	 * Sets the opera local service.
	 *
	 * @param operaLocalService the opera local service
	 */
	public void setOperaLocalService(
		it.cloudtec.biblioteca.service.OperaLocalService operaLocalService) {

		this.operaLocalService = operaLocalService;
	}

	/**
	 * Returns the opera remote service.
	 *
	 * @return the opera remote service
	 */
	public OperaService getOperaService() {
		return operaService;
	}

	/**
	 * Sets the opera remote service.
	 *
	 * @param operaService the opera remote service
	 */
	public void setOperaService(OperaService operaService) {
		this.operaService = operaService;
	}

	/**
	 * Returns the opera persistence.
	 *
	 * @return the opera persistence
	 */
	public OperaPersistence getOperaPersistence() {
		return operaPersistence;
	}

	/**
	 * Sets the opera persistence.
	 *
	 * @param operaPersistence the opera persistence
	 */
	public void setOperaPersistence(OperaPersistence operaPersistence) {
		this.operaPersistence = operaPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService
		getCounterLocalService() {

		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService
			counterLocalService) {

		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService
		getClassNameLocalService() {

		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService
			classNameLocalService) {

		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name remote service.
	 *
	 * @return the class name remote service
	 */
	public com.liferay.portal.kernel.service.ClassNameService
		getClassNameService() {

		return classNameService;
	}

	/**
	 * Sets the class name remote service.
	 *
	 * @param classNameService the class name remote service
	 */
	public void setClassNameService(
		com.liferay.portal.kernel.service.ClassNameService classNameService) {

		this.classNameService = classNameService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {

		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService
		getResourceLocalService() {

		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService
			resourceLocalService) {

		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService
		getUserLocalService() {

		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {

		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.kernel.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.kernel.service.UserService userService) {

		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
	}

	public void destroy() {
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return OperaService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Opera.class;
	}

	protected String getModelClassName() {
		return Opera.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = operaPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(
		type = it.cloudtec.biblioteca.service.AutoreLocalService.class
	)
	protected it.cloudtec.biblioteca.service.AutoreLocalService
		autoreLocalService;

	@BeanReference(type = it.cloudtec.biblioteca.service.AutoreService.class)
	protected it.cloudtec.biblioteca.service.AutoreService autoreService;

	@BeanReference(type = AutorePersistence.class)
	protected AutorePersistence autorePersistence;

	@BeanReference(
		type = it.cloudtec.biblioteca.service.OperaLocalService.class
	)
	protected it.cloudtec.biblioteca.service.OperaLocalService
		operaLocalService;

	@BeanReference(type = OperaService.class)
	protected OperaService operaService;

	@BeanReference(type = OperaPersistence.class)
	protected OperaPersistence operaPersistence;

	@ServiceReference(
		type = com.liferay.counter.kernel.service.CounterLocalService.class
	)
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.ClassNameLocalService.class
	)
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.ClassNameService.class
	)
	protected com.liferay.portal.kernel.service.ClassNameService
		classNameService;

	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.ResourceLocalService.class
	)
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.UserLocalService.class
	)
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.UserService.class
	)
	protected com.liferay.portal.kernel.service.UserService userService;

	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;

}