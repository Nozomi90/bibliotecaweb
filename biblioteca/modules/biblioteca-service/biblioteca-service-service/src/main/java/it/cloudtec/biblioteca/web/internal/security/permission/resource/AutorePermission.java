package it.cloudtec.biblioteca.web.internal.security.permission.resource;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import it.cloudtec.biblioteca.model.Autore;

@Component(immediate = true)
public class AutorePermission {

    public static boolean contains(
            PermissionChecker permissionChecker, Autore autore,
            String actionId)
        throws PortalException {

        return _autoreFolderModelResourcePermission.contains(
            permissionChecker, autore, actionId);
    }

    public static boolean contains(
            PermissionChecker permissionChecker, long entryId, String actionId)
        throws PortalException {

        return _autoreFolderModelResourcePermission.contains(
            permissionChecker, entryId, actionId);
    }

    @Reference(
        target = "(model.class.name=it.cloudtec.biblioteca.model.Autore)",
        unbind = "-"
    )
    protected void setEntryModelPermission(
        ModelResourcePermission<Autore> modelResourcePermission) {

    	_autoreFolderModelResourcePermission = modelResourcePermission;
    }

    private static ModelResourcePermission<Autore>
        _autoreFolderModelResourcePermission;

}