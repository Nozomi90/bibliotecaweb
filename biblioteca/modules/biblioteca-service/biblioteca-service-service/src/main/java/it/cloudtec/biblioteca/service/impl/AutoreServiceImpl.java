/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service.impl;

import org.osgi.service.component.annotations.Reference;

import it.cloudtec.biblioteca.constants.AutoreConstants;
import it.cloudtec.biblioteca.internal.security.permission.resource.ActionKeysPersonal;
import it.cloudtec.biblioteca.model.Autore;
import it.cloudtec.biblioteca.service.AutoreLocalService;
import it.cloudtec.biblioteca.service.base.AutoreServiceBaseImpl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionFactory;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionHelper;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermissionFactory;
import com.liferay.portal.kernel.service.ServiceContext;

/**
 * The implementation of the autore remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.cloudtec.biblioteca.service.AutoreService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AutoreServiceBaseImpl
 */
public class AutoreServiceImpl extends AutoreServiceBaseImpl {

	@Reference
	private AutoreLocalService autoreLocalService;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>com.cloudtec.biblioteca.service.AutoreServiceUtil</code> to access the autore remote service.
	 */
	@Override
	public Autore addAutoreEsterno(String nome, String categoria, ServiceContext serviceContext) 
			throws SystemException, PortalException {

		_autoreFolderModelResourcePermission.check(
				getPermissionChecker(), serviceContext.getScopeGroupId(),
				ActionKeysPersonal.AGGIUNGI_AUTORE);

		return autoreLocalService.nuovoAutore(serviceContext.getCompanyId(), 
				serviceContext.getScopeGroupId(), serviceContext.getUserId(), nome, categoria, serviceContext);
	}



	private static volatile ModelResourcePermission<Autore>
	_autoreFolderModelResourcePermission =
	ModelResourcePermissionFactory.getInstance(
			AutoreServiceImpl.class,
			"_autoreFolderModelResourcePermission", Autore.class);

	private static volatile PortletResourcePermission
	_portletResourcePermission =
	PortletResourcePermissionFactory.getInstance(
			AutoreServiceImpl.class, "_portletResourcePermission",
			AutoreConstants.RESOURCE_NAME);


}