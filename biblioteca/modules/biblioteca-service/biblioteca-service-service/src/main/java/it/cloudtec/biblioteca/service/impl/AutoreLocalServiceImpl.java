/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service.impl;

import java.util.Calendar;
import java.util.Date;

import it.cloudtec.biblioteca.model.Autore;
import it.cloudtec.biblioteca.service.base.AutoreLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;

/**
 * The implementation of the autore local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.cloudtec.biblioteca.service.AutoreLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AutoreLocalServiceBaseImpl
 */
public class AutoreLocalServiceImpl extends AutoreLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.cloudtec.biblioteca.service.AutoreLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.cloudtec.biblioteca.service.AutoreLocalServiceUtil</code>.
	 */



	public Autore nuovoAutore( long companyId, long groupId, long userId, String nome, String categoria, ServiceContext serviceContext ) throws PortalException, SystemException {

		User user = userLocalService.getUser(userId);
				
		Date now = Calendar.getInstance().getTime();
		
		Autore autore = autorePersistence.create(counterLocalService.increment(Autore.class.getName()));

		autore.setNome(nome);
		autore.setNumeroOpere(0);
		autore.setCategoria(categoria);
		autore.setCompanyId(companyId);
		autore.setGroupId(groupId);
		autore.setUserId(user.getUserId());
		autore.setCreateDate(serviceContext.getCreateDate(now));
		autore.setModifiedDate(serviceContext.getModifiedDate(now));

		autore = autorePersistence.update(autore);

		resourceLocalService.addResources(
				autore.getCompanyId(), autore.getGroupId(), autore.getUserId(),
				Autore.class.getName(), autore.getAutoreId(), false,
				true, true);
		return autore;


	}

}