create table autore (
	uuid_ VARCHAR(75) null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	autoreId LONG not null primary key,
	nome VARCHAR(75) null,
	numeroOpere LONG,
	categoria VARCHAR(75) null
);

create table opera (
	uuid_ VARCHAR(75) null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	operaId LONG not null primary key,
	autoreId LONG,
	titolo VARCHAR(75) null,
	categoria VARCHAR(75) null
);