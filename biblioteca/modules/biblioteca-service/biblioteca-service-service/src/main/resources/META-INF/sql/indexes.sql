create index IX_FDB38A42 on autore (groupId);
create index IX_6AEF375C on autore (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E255E4DE on autore (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_961ADC8D on opera (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_D9DB224F on opera (uuid_[$COLUMN_LENGTH:75$], groupId);