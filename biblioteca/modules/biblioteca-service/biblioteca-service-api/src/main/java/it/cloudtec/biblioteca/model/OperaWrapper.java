/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Opera}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Opera
 * @generated
 */
@ProviderType
public class OperaWrapper implements Opera, ModelWrapper<Opera> {

	public OperaWrapper(Opera opera) {
		_opera = opera;
	}

	@Override
	public Class<?> getModelClass() {
		return Opera.class;
	}

	@Override
	public String getModelClassName() {
		return Opera.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("operaId", getOperaId());
		attributes.put("autoreId", getAutoreId());
		attributes.put("titolo", getTitolo());
		attributes.put("categoria", getCategoria());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long operaId = (Long)attributes.get("operaId");

		if (operaId != null) {
			setOperaId(operaId);
		}

		Long autoreId = (Long)attributes.get("autoreId");

		if (autoreId != null) {
			setAutoreId(autoreId);
		}

		String titolo = (String)attributes.get("titolo");

		if (titolo != null) {
			setTitolo(titolo);
		}

		String categoria = (String)attributes.get("categoria");

		if (categoria != null) {
			setCategoria(categoria);
		}
	}

	@Override
	public Object clone() {
		return new OperaWrapper((Opera)_opera.clone());
	}

	@Override
	public int compareTo(it.cloudtec.biblioteca.model.Opera opera) {
		return _opera.compareTo(opera);
	}

	/**
	 * Returns the autore ID of this opera.
	 *
	 * @return the autore ID of this opera
	 */
	@Override
	public long getAutoreId() {
		return _opera.getAutoreId();
	}

	/**
	 * Returns the categoria of this opera.
	 *
	 * @return the categoria of this opera
	 */
	@Override
	public String getCategoria() {
		return _opera.getCategoria();
	}

	/**
	 * Returns the company ID of this opera.
	 *
	 * @return the company ID of this opera
	 */
	@Override
	public long getCompanyId() {
		return _opera.getCompanyId();
	}

	/**
	 * Returns the create date of this opera.
	 *
	 * @return the create date of this opera
	 */
	@Override
	public Date getCreateDate() {
		return _opera.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _opera.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this opera.
	 *
	 * @return the group ID of this opera
	 */
	@Override
	public long getGroupId() {
		return _opera.getGroupId();
	}

	/**
	 * Returns the modified date of this opera.
	 *
	 * @return the modified date of this opera
	 */
	@Override
	public Date getModifiedDate() {
		return _opera.getModifiedDate();
	}

	/**
	 * Returns the opera ID of this opera.
	 *
	 * @return the opera ID of this opera
	 */
	@Override
	public long getOperaId() {
		return _opera.getOperaId();
	}

	/**
	 * Returns the primary key of this opera.
	 *
	 * @return the primary key of this opera
	 */
	@Override
	public long getPrimaryKey() {
		return _opera.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _opera.getPrimaryKeyObj();
	}

	/**
	 * Returns the status of this opera.
	 *
	 * @return the status of this opera
	 */
	@Override
	public int getStatus() {
		return _opera.getStatus();
	}

	/**
	 * Returns the status by user ID of this opera.
	 *
	 * @return the status by user ID of this opera
	 */
	@Override
	public long getStatusByUserId() {
		return _opera.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this opera.
	 *
	 * @return the status by user name of this opera
	 */
	@Override
	public String getStatusByUserName() {
		return _opera.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this opera.
	 *
	 * @return the status by user uuid of this opera
	 */
	@Override
	public String getStatusByUserUuid() {
		return _opera.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this opera.
	 *
	 * @return the status date of this opera
	 */
	@Override
	public Date getStatusDate() {
		return _opera.getStatusDate();
	}

	/**
	 * Returns the titolo of this opera.
	 *
	 * @return the titolo of this opera
	 */
	@Override
	public String getTitolo() {
		return _opera.getTitolo();
	}

	/**
	 * Returns the user ID of this opera.
	 *
	 * @return the user ID of this opera
	 */
	@Override
	public long getUserId() {
		return _opera.getUserId();
	}

	/**
	 * Returns the user name of this opera.
	 *
	 * @return the user name of this opera
	 */
	@Override
	public String getUserName() {
		return _opera.getUserName();
	}

	/**
	 * Returns the user uuid of this opera.
	 *
	 * @return the user uuid of this opera
	 */
	@Override
	public String getUserUuid() {
		return _opera.getUserUuid();
	}

	/**
	 * Returns the uuid of this opera.
	 *
	 * @return the uuid of this opera
	 */
	@Override
	public String getUuid() {
		return _opera.getUuid();
	}

	@Override
	public int hashCode() {
		return _opera.hashCode();
	}

	/**
	 * Returns <code>true</code> if this opera is approved.
	 *
	 * @return <code>true</code> if this opera is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return _opera.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _opera.isCachedModel();
	}

	/**
	 * Returns <code>true</code> if this opera is denied.
	 *
	 * @return <code>true</code> if this opera is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return _opera.isDenied();
	}

	/**
	 * Returns <code>true</code> if this opera is a draft.
	 *
	 * @return <code>true</code> if this opera is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return _opera.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _opera.isEscapedModel();
	}

	/**
	 * Returns <code>true</code> if this opera is expired.
	 *
	 * @return <code>true</code> if this opera is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return _opera.isExpired();
	}

	/**
	 * Returns <code>true</code> if this opera is inactive.
	 *
	 * @return <code>true</code> if this opera is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return _opera.isInactive();
	}

	/**
	 * Returns <code>true</code> if this opera is incomplete.
	 *
	 * @return <code>true</code> if this opera is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return _opera.isIncomplete();
	}

	@Override
	public boolean isNew() {
		return _opera.isNew();
	}

	/**
	 * Returns <code>true</code> if this opera is pending.
	 *
	 * @return <code>true</code> if this opera is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return _opera.isPending();
	}

	/**
	 * Returns <code>true</code> if this opera is scheduled.
	 *
	 * @return <code>true</code> if this opera is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return _opera.isScheduled();
	}

	@Override
	public void persist() {
		_opera.persist();
	}

	/**
	 * Sets the autore ID of this opera.
	 *
	 * @param autoreId the autore ID of this opera
	 */
	@Override
	public void setAutoreId(long autoreId) {
		_opera.setAutoreId(autoreId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_opera.setCachedModel(cachedModel);
	}

	/**
	 * Sets the categoria of this opera.
	 *
	 * @param categoria the categoria of this opera
	 */
	@Override
	public void setCategoria(String categoria) {
		_opera.setCategoria(categoria);
	}

	/**
	 * Sets the company ID of this opera.
	 *
	 * @param companyId the company ID of this opera
	 */
	@Override
	public void setCompanyId(long companyId) {
		_opera.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this opera.
	 *
	 * @param createDate the create date of this opera
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_opera.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_opera.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_opera.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_opera.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this opera.
	 *
	 * @param groupId the group ID of this opera
	 */
	@Override
	public void setGroupId(long groupId) {
		_opera.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this opera.
	 *
	 * @param modifiedDate the modified date of this opera
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_opera.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_opera.setNew(n);
	}

	/**
	 * Sets the opera ID of this opera.
	 *
	 * @param operaId the opera ID of this opera
	 */
	@Override
	public void setOperaId(long operaId) {
		_opera.setOperaId(operaId);
	}

	/**
	 * Sets the primary key of this opera.
	 *
	 * @param primaryKey the primary key of this opera
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_opera.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_opera.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the status of this opera.
	 *
	 * @param status the status of this opera
	 */
	@Override
	public void setStatus(int status) {
		_opera.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this opera.
	 *
	 * @param statusByUserId the status by user ID of this opera
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_opera.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this opera.
	 *
	 * @param statusByUserName the status by user name of this opera
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		_opera.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this opera.
	 *
	 * @param statusByUserUuid the status by user uuid of this opera
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_opera.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this opera.
	 *
	 * @param statusDate the status date of this opera
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		_opera.setStatusDate(statusDate);
	}

	/**
	 * Sets the titolo of this opera.
	 *
	 * @param titolo the titolo of this opera
	 */
	@Override
	public void setTitolo(String titolo) {
		_opera.setTitolo(titolo);
	}

	/**
	 * Sets the user ID of this opera.
	 *
	 * @param userId the user ID of this opera
	 */
	@Override
	public void setUserId(long userId) {
		_opera.setUserId(userId);
	}

	/**
	 * Sets the user name of this opera.
	 *
	 * @param userName the user name of this opera
	 */
	@Override
	public void setUserName(String userName) {
		_opera.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this opera.
	 *
	 * @param userUuid the user uuid of this opera
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_opera.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this opera.
	 *
	 * @param uuid the uuid of this opera
	 */
	@Override
	public void setUuid(String uuid) {
		_opera.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel
		<it.cloudtec.biblioteca.model.Opera> toCacheModel() {

		return _opera.toCacheModel();
	}

	@Override
	public it.cloudtec.biblioteca.model.Opera toEscapedModel() {
		return new OperaWrapper(_opera.toEscapedModel());
	}

	@Override
	public String toString() {
		return _opera.toString();
	}

	@Override
	public it.cloudtec.biblioteca.model.Opera toUnescapedModel() {
		return new OperaWrapper(_opera.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _opera.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OperaWrapper)) {
			return false;
		}

		OperaWrapper operaWrapper = (OperaWrapper)obj;

		if (Objects.equals(_opera, operaWrapper._opera)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _opera.getStagedModelType();
	}

	@Override
	public Opera getWrappedModel() {
		return _opera;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _opera.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _opera.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_opera.resetOriginalValues();
	}

	private final Opera _opera;

}