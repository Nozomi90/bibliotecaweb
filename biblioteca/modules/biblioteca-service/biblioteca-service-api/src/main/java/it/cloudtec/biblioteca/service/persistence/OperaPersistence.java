/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.cloudtec.biblioteca.exception.NoSuchOperaException;
import it.cloudtec.biblioteca.model.Opera;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the opera service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OperaUtil
 * @generated
 */
@ProviderType
public interface OperaPersistence extends BasePersistence<Opera> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OperaUtil} to access the opera persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, Opera> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the operas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching operas
	 */
	public java.util.List<Opera> findByUuid(String uuid);

	/**
	 * Returns a range of all the operas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @return the range of matching operas
	 */
	public java.util.List<Opera> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the operas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching operas
	 */
	public java.util.List<Opera> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns an ordered range of all the operas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching operas
	 */
	public java.util.List<Opera> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first opera in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching opera
	 * @throws NoSuchOperaException if a matching opera could not be found
	 */
	public Opera findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Returns the first opera in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns the last opera in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching opera
	 * @throws NoSuchOperaException if a matching opera could not be found
	 */
	public Opera findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Returns the last opera in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns the operas before and after the current opera in the ordered set where uuid = &#63;.
	 *
	 * @param operaId the primary key of the current opera
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next opera
	 * @throws NoSuchOperaException if a opera with the primary key could not be found
	 */
	public Opera[] findByUuid_PrevAndNext(
			long operaId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Removes all the operas where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of operas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching operas
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the opera where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchOperaException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching opera
	 * @throws NoSuchOperaException if a matching opera could not be found
	 */
	public Opera findByUUID_G(String uuid, long groupId)
		throws NoSuchOperaException;

	/**
	 * Returns the opera where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the opera where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the opera where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the opera that was removed
	 */
	public Opera removeByUUID_G(String uuid, long groupId)
		throws NoSuchOperaException;

	/**
	 * Returns the number of operas where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching operas
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the operas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching operas
	 */
	public java.util.List<Opera> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the operas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @return the range of matching operas
	 */
	public java.util.List<Opera> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the operas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching operas
	 */
	public java.util.List<Opera> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns an ordered range of all the operas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching operas
	 */
	public java.util.List<Opera> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first opera in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching opera
	 * @throws NoSuchOperaException if a matching opera could not be found
	 */
	public Opera findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Returns the first opera in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns the last opera in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching opera
	 * @throws NoSuchOperaException if a matching opera could not be found
	 */
	public Opera findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Returns the last opera in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public Opera fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns the operas before and after the current opera in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param operaId the primary key of the current opera
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next opera
	 * @throws NoSuchOperaException if a opera with the primary key could not be found
	 */
	public Opera[] findByUuid_C_PrevAndNext(
			long operaId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Opera>
				orderByComparator)
		throws NoSuchOperaException;

	/**
	 * Removes all the operas where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of operas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching operas
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Caches the opera in the entity cache if it is enabled.
	 *
	 * @param opera the opera
	 */
	public void cacheResult(Opera opera);

	/**
	 * Caches the operas in the entity cache if it is enabled.
	 *
	 * @param operas the operas
	 */
	public void cacheResult(java.util.List<Opera> operas);

	/**
	 * Creates a new opera with the primary key. Does not add the opera to the database.
	 *
	 * @param operaId the primary key for the new opera
	 * @return the new opera
	 */
	public Opera create(long operaId);

	/**
	 * Removes the opera with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param operaId the primary key of the opera
	 * @return the opera that was removed
	 * @throws NoSuchOperaException if a opera with the primary key could not be found
	 */
	public Opera remove(long operaId) throws NoSuchOperaException;

	public Opera updateImpl(Opera opera);

	/**
	 * Returns the opera with the primary key or throws a <code>NoSuchOperaException</code> if it could not be found.
	 *
	 * @param operaId the primary key of the opera
	 * @return the opera
	 * @throws NoSuchOperaException if a opera with the primary key could not be found
	 */
	public Opera findByPrimaryKey(long operaId) throws NoSuchOperaException;

	/**
	 * Returns the opera with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param operaId the primary key of the opera
	 * @return the opera, or <code>null</code> if a opera with the primary key could not be found
	 */
	public Opera fetchByPrimaryKey(long operaId);

	/**
	 * Returns all the operas.
	 *
	 * @return the operas
	 */
	public java.util.List<Opera> findAll();

	/**
	 * Returns a range of all the operas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @return the range of operas
	 */
	public java.util.List<Opera> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the operas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of operas
	 */
	public java.util.List<Opera> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator);

	/**
	 * Returns an ordered range of all the operas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of operas
	 */
	public java.util.List<Opera> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Opera>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the operas from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of operas.
	 *
	 * @return the number of operas
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}