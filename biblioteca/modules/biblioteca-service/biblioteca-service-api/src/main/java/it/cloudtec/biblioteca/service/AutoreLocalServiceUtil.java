/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Autore. This utility wraps
 * <code>it.cloudtec.biblioteca.service.impl.AutoreLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AutoreLocalService
 * @generated
 */
@ProviderType
public class AutoreLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.cloudtec.biblioteca.service.impl.AutoreLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the autore to the database. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was added
	 */
	public static it.cloudtec.biblioteca.model.Autore addAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return getService().addAutore(autore);
	}

	/**
	 * Creates a new autore with the primary key. Does not add the autore to the database.
	 *
	 * @param autoreId the primary key for the new autore
	 * @return the new autore
	 */
	public static it.cloudtec.biblioteca.model.Autore createAutore(
		long autoreId) {

		return getService().createAutore(autoreId);
	}

	/**
	 * Deletes the autore from the database. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was removed
	 */
	public static it.cloudtec.biblioteca.model.Autore deleteAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return getService().deleteAutore(autore);
	}

	/**
	 * Deletes the autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore that was removed
	 * @throws PortalException if a autore with the primary key could not be found
	 */
	public static it.cloudtec.biblioteca.model.Autore deleteAutore(
			long autoreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteAutore(autoreId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.cloudtec.biblioteca.model.Autore fetchAutore(
		long autoreId) {

		return getService().fetchAutore(autoreId);
	}

	/**
	 * Returns the autore matching the UUID and group.
	 *
	 * @param uuid the autore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching autore, or <code>null</code> if a matching autore could not be found
	 */
	public static it.cloudtec.biblioteca.model.Autore
		fetchAutoreByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchAutoreByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the autore with the primary key.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore
	 * @throws PortalException if a autore with the primary key could not be found
	 */
	public static it.cloudtec.biblioteca.model.Autore getAutore(long autoreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAutore(autoreId);
	}

	/**
	 * Returns the autore matching the UUID and group.
	 *
	 * @param uuid the autore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching autore
	 * @throws PortalException if a matching autore could not be found
	 */
	public static it.cloudtec.biblioteca.model.Autore getAutoreByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAutoreByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @return the range of autores
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Autore>
		getAutores(int start, int end) {

		return getService().getAutores(start, end);
	}

	/**
	 * Returns all the autores matching the UUID and company.
	 *
	 * @param uuid the UUID of the autores
	 * @param companyId the primary key of the company
	 * @return the matching autores, or an empty list if no matches were found
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Autore>
		getAutoresByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getAutoresByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of autores matching the UUID and company.
	 *
	 * @param uuid the UUID of the autores
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching autores, or an empty list if no matches were found
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Autore>
		getAutoresByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.cloudtec.biblioteca.model.Autore> orderByComparator) {

		return getService().getAutoresByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of autores.
	 *
	 * @return the number of autores
	 */
	public static int getAutoresCount() {
		return getService().getAutoresCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static it.cloudtec.biblioteca.model.Autore nuovoAutore(
			long companyId, long groupId, long userId, String nome,
			String categoria,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return getService().nuovoAutore(
			companyId, groupId, userId, nome, categoria, serviceContext);
	}

	/**
	 * Updates the autore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was updated
	 */
	public static it.cloudtec.biblioteca.model.Autore updateAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return getService().updateAutore(autore);
	}

	public static AutoreLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AutoreLocalService, AutoreLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AutoreLocalService.class);

		ServiceTracker<AutoreLocalService, AutoreLocalService> serviceTracker =
			new ServiceTracker<AutoreLocalService, AutoreLocalService>(
				bundle.getBundleContext(), AutoreLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}