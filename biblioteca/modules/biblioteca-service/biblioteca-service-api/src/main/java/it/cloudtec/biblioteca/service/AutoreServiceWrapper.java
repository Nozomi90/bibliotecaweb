/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AutoreService}.
 *
 * @author Brian Wing Shun Chan
 * @see AutoreService
 * @generated
 */
@ProviderType
public class AutoreServiceWrapper
	implements AutoreService, ServiceWrapper<AutoreService> {

	public AutoreServiceWrapper(AutoreService autoreService) {
		_autoreService = autoreService;
	}

	@Override
	public it.cloudtec.biblioteca.model.Autore addAutoreEsterno(
			String nome, String categoria,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _autoreService.addAutoreEsterno(nome, categoria, serviceContext);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _autoreService.getOSGiServiceIdentifier();
	}

	@Override
	public AutoreService getWrappedService() {
		return _autoreService;
	}

	@Override
	public void setWrappedService(AutoreService autoreService) {
		_autoreService = autoreService;
	}

	private AutoreService _autoreService;

}