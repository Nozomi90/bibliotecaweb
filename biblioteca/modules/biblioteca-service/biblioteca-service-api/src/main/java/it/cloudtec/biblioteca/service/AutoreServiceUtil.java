/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Autore. This utility wraps
 * <code>it.cloudtec.biblioteca.service.impl.AutoreServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see AutoreService
 * @generated
 */
@ProviderType
public class AutoreServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.cloudtec.biblioteca.service.impl.AutoreServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static it.cloudtec.biblioteca.model.Autore addAutoreEsterno(
			String nome, String categoria,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return getService().addAutoreEsterno(nome, categoria, serviceContext);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static AutoreService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AutoreService, AutoreService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AutoreService.class);

		ServiceTracker<AutoreService, AutoreService> serviceTracker =
			new ServiceTracker<AutoreService, AutoreService>(
				bundle.getBundleContext(), AutoreService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}