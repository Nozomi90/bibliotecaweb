/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AutoreLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AutoreLocalService
 * @generated
 */
@ProviderType
public class AutoreLocalServiceWrapper
	implements AutoreLocalService, ServiceWrapper<AutoreLocalService> {

	public AutoreLocalServiceWrapper(AutoreLocalService autoreLocalService) {
		_autoreLocalService = autoreLocalService;
	}

	/**
	 * Adds the autore to the database. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was added
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore addAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return _autoreLocalService.addAutore(autore);
	}

	/**
	 * Creates a new autore with the primary key. Does not add the autore to the database.
	 *
	 * @param autoreId the primary key for the new autore
	 * @return the new autore
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore createAutore(long autoreId) {
		return _autoreLocalService.createAutore(autoreId);
	}

	/**
	 * Deletes the autore from the database. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was removed
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore deleteAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return _autoreLocalService.deleteAutore(autore);
	}

	/**
	 * Deletes the autore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore that was removed
	 * @throws PortalException if a autore with the primary key could not be found
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore deleteAutore(long autoreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _autoreLocalService.deleteAutore(autoreId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _autoreLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _autoreLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _autoreLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _autoreLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _autoreLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _autoreLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _autoreLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public it.cloudtec.biblioteca.model.Autore fetchAutore(long autoreId) {
		return _autoreLocalService.fetchAutore(autoreId);
	}

	/**
	 * Returns the autore matching the UUID and group.
	 *
	 * @param uuid the autore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching autore, or <code>null</code> if a matching autore could not be found
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore fetchAutoreByUuidAndGroupId(
		String uuid, long groupId) {

		return _autoreLocalService.fetchAutoreByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _autoreLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the autore with the primary key.
	 *
	 * @param autoreId the primary key of the autore
	 * @return the autore
	 * @throws PortalException if a autore with the primary key could not be found
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore getAutore(long autoreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _autoreLocalService.getAutore(autoreId);
	}

	/**
	 * Returns the autore matching the UUID and group.
	 *
	 * @param uuid the autore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching autore
	 * @throws PortalException if a matching autore could not be found
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore getAutoreByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _autoreLocalService.getAutoreByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the autores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.AutoreModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @return the range of autores
	 */
	@Override
	public java.util.List<it.cloudtec.biblioteca.model.Autore> getAutores(
		int start, int end) {

		return _autoreLocalService.getAutores(start, end);
	}

	/**
	 * Returns all the autores matching the UUID and company.
	 *
	 * @param uuid the UUID of the autores
	 * @param companyId the primary key of the company
	 * @return the matching autores, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<it.cloudtec.biblioteca.model.Autore>
		getAutoresByUuidAndCompanyId(String uuid, long companyId) {

		return _autoreLocalService.getAutoresByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of autores matching the UUID and company.
	 *
	 * @param uuid the UUID of the autores
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of autores
	 * @param end the upper bound of the range of autores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching autores, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<it.cloudtec.biblioteca.model.Autore>
		getAutoresByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.cloudtec.biblioteca.model.Autore> orderByComparator) {

		return _autoreLocalService.getAutoresByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of autores.
	 *
	 * @return the number of autores
	 */
	@Override
	public int getAutoresCount() {
		return _autoreLocalService.getAutoresCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _autoreLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _autoreLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _autoreLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _autoreLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public it.cloudtec.biblioteca.model.Autore nuovoAutore(
			long companyId, long groupId, long userId, String nome,
			String categoria,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _autoreLocalService.nuovoAutore(
			companyId, groupId, userId, nome, categoria, serviceContext);
	}

	/**
	 * Updates the autore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param autore the autore
	 * @return the autore that was updated
	 */
	@Override
	public it.cloudtec.biblioteca.model.Autore updateAutore(
		it.cloudtec.biblioteca.model.Autore autore) {

		return _autoreLocalService.updateAutore(autore);
	}

	@Override
	public AutoreLocalService getWrappedService() {
		return _autoreLocalService;
	}

	@Override
	public void setWrappedService(AutoreLocalService autoreLocalService) {
		_autoreLocalService = autoreLocalService;
	}

	private AutoreLocalService _autoreLocalService;

}