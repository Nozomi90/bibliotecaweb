package it.cloudtec.biblioteca.constants;

/**
 * @author Cristian
 */
public class BibliotecaWebPortletKeys {

	public static final String BIBLIOTECAWEB =
		"it_cloudtec_biblioteca_portlet_BibliotecaWebPortlet";

}