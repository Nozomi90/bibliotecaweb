/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Autore}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Autore
 * @generated
 */
@ProviderType
public class AutoreWrapper implements Autore, ModelWrapper<Autore> {

	public AutoreWrapper(Autore autore) {
		_autore = autore;
	}

	@Override
	public Class<?> getModelClass() {
		return Autore.class;
	}

	@Override
	public String getModelClassName() {
		return Autore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("autoreId", getAutoreId());
		attributes.put("nome", getNome());
		attributes.put("numeroOpere", getNumeroOpere());
		attributes.put("categoria", getCategoria());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long autoreId = (Long)attributes.get("autoreId");

		if (autoreId != null) {
			setAutoreId(autoreId);
		}

		String nome = (String)attributes.get("nome");

		if (nome != null) {
			setNome(nome);
		}

		Long numeroOpere = (Long)attributes.get("numeroOpere");

		if (numeroOpere != null) {
			setNumeroOpere(numeroOpere);
		}

		String categoria = (String)attributes.get("categoria");

		if (categoria != null) {
			setCategoria(categoria);
		}
	}

	@Override
	public Object clone() {
		return new AutoreWrapper((Autore)_autore.clone());
	}

	@Override
	public int compareTo(it.cloudtec.biblioteca.model.Autore autore) {
		return _autore.compareTo(autore);
	}

	/**
	 * Returns the autore ID of this autore.
	 *
	 * @return the autore ID of this autore
	 */
	@Override
	public long getAutoreId() {
		return _autore.getAutoreId();
	}

	/**
	 * Returns the categoria of this autore.
	 *
	 * @return the categoria of this autore
	 */
	@Override
	public String getCategoria() {
		return _autore.getCategoria();
	}

	/**
	 * Returns the company ID of this autore.
	 *
	 * @return the company ID of this autore
	 */
	@Override
	public long getCompanyId() {
		return _autore.getCompanyId();
	}

	/**
	 * Returns the create date of this autore.
	 *
	 * @return the create date of this autore
	 */
	@Override
	public Date getCreateDate() {
		return _autore.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _autore.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this autore.
	 *
	 * @return the group ID of this autore
	 */
	@Override
	public long getGroupId() {
		return _autore.getGroupId();
	}

	/**
	 * Returns the modified date of this autore.
	 *
	 * @return the modified date of this autore
	 */
	@Override
	public Date getModifiedDate() {
		return _autore.getModifiedDate();
	}

	/**
	 * Returns the nome of this autore.
	 *
	 * @return the nome of this autore
	 */
	@Override
	public String getNome() {
		return _autore.getNome();
	}

	/**
	 * Returns the numero opere of this autore.
	 *
	 * @return the numero opere of this autore
	 */
	@Override
	public long getNumeroOpere() {
		return _autore.getNumeroOpere();
	}

	/**
	 * Returns the primary key of this autore.
	 *
	 * @return the primary key of this autore
	 */
	@Override
	public long getPrimaryKey() {
		return _autore.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _autore.getPrimaryKeyObj();
	}

	/**
	 * Returns the status of this autore.
	 *
	 * @return the status of this autore
	 */
	@Override
	public int getStatus() {
		return _autore.getStatus();
	}

	/**
	 * Returns the status by user ID of this autore.
	 *
	 * @return the status by user ID of this autore
	 */
	@Override
	public long getStatusByUserId() {
		return _autore.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this autore.
	 *
	 * @return the status by user name of this autore
	 */
	@Override
	public String getStatusByUserName() {
		return _autore.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this autore.
	 *
	 * @return the status by user uuid of this autore
	 */
	@Override
	public String getStatusByUserUuid() {
		return _autore.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this autore.
	 *
	 * @return the status date of this autore
	 */
	@Override
	public Date getStatusDate() {
		return _autore.getStatusDate();
	}

	/**
	 * Returns the user ID of this autore.
	 *
	 * @return the user ID of this autore
	 */
	@Override
	public long getUserId() {
		return _autore.getUserId();
	}

	/**
	 * Returns the user name of this autore.
	 *
	 * @return the user name of this autore
	 */
	@Override
	public String getUserName() {
		return _autore.getUserName();
	}

	/**
	 * Returns the user uuid of this autore.
	 *
	 * @return the user uuid of this autore
	 */
	@Override
	public String getUserUuid() {
		return _autore.getUserUuid();
	}

	/**
	 * Returns the uuid of this autore.
	 *
	 * @return the uuid of this autore
	 */
	@Override
	public String getUuid() {
		return _autore.getUuid();
	}

	@Override
	public int hashCode() {
		return _autore.hashCode();
	}

	/**
	 * Returns <code>true</code> if this autore is approved.
	 *
	 * @return <code>true</code> if this autore is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return _autore.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _autore.isCachedModel();
	}

	/**
	 * Returns <code>true</code> if this autore is denied.
	 *
	 * @return <code>true</code> if this autore is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return _autore.isDenied();
	}

	/**
	 * Returns <code>true</code> if this autore is a draft.
	 *
	 * @return <code>true</code> if this autore is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return _autore.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _autore.isEscapedModel();
	}

	/**
	 * Returns <code>true</code> if this autore is expired.
	 *
	 * @return <code>true</code> if this autore is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return _autore.isExpired();
	}

	/**
	 * Returns <code>true</code> if this autore is inactive.
	 *
	 * @return <code>true</code> if this autore is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return _autore.isInactive();
	}

	/**
	 * Returns <code>true</code> if this autore is incomplete.
	 *
	 * @return <code>true</code> if this autore is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return _autore.isIncomplete();
	}

	@Override
	public boolean isNew() {
		return _autore.isNew();
	}

	/**
	 * Returns <code>true</code> if this autore is pending.
	 *
	 * @return <code>true</code> if this autore is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return _autore.isPending();
	}

	/**
	 * Returns <code>true</code> if this autore is scheduled.
	 *
	 * @return <code>true</code> if this autore is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return _autore.isScheduled();
	}

	@Override
	public void persist() {
		_autore.persist();
	}

	/**
	 * Sets the autore ID of this autore.
	 *
	 * @param autoreId the autore ID of this autore
	 */
	@Override
	public void setAutoreId(long autoreId) {
		_autore.setAutoreId(autoreId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_autore.setCachedModel(cachedModel);
	}

	/**
	 * Sets the categoria of this autore.
	 *
	 * @param categoria the categoria of this autore
	 */
	@Override
	public void setCategoria(String categoria) {
		_autore.setCategoria(categoria);
	}

	/**
	 * Sets the company ID of this autore.
	 *
	 * @param companyId the company ID of this autore
	 */
	@Override
	public void setCompanyId(long companyId) {
		_autore.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this autore.
	 *
	 * @param createDate the create date of this autore
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_autore.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_autore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_autore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_autore.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this autore.
	 *
	 * @param groupId the group ID of this autore
	 */
	@Override
	public void setGroupId(long groupId) {
		_autore.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this autore.
	 *
	 * @param modifiedDate the modified date of this autore
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_autore.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_autore.setNew(n);
	}

	/**
	 * Sets the nome of this autore.
	 *
	 * @param nome the nome of this autore
	 */
	@Override
	public void setNome(String nome) {
		_autore.setNome(nome);
	}

	/**
	 * Sets the numero opere of this autore.
	 *
	 * @param numeroOpere the numero opere of this autore
	 */
	@Override
	public void setNumeroOpere(long numeroOpere) {
		_autore.setNumeroOpere(numeroOpere);
	}

	/**
	 * Sets the primary key of this autore.
	 *
	 * @param primaryKey the primary key of this autore
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_autore.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_autore.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the status of this autore.
	 *
	 * @param status the status of this autore
	 */
	@Override
	public void setStatus(int status) {
		_autore.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this autore.
	 *
	 * @param statusByUserId the status by user ID of this autore
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_autore.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this autore.
	 *
	 * @param statusByUserName the status by user name of this autore
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		_autore.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this autore.
	 *
	 * @param statusByUserUuid the status by user uuid of this autore
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_autore.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this autore.
	 *
	 * @param statusDate the status date of this autore
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		_autore.setStatusDate(statusDate);
	}

	/**
	 * Sets the user ID of this autore.
	 *
	 * @param userId the user ID of this autore
	 */
	@Override
	public void setUserId(long userId) {
		_autore.setUserId(userId);
	}

	/**
	 * Sets the user name of this autore.
	 *
	 * @param userName the user name of this autore
	 */
	@Override
	public void setUserName(String userName) {
		_autore.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this autore.
	 *
	 * @param userUuid the user uuid of this autore
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_autore.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this autore.
	 *
	 * @param uuid the uuid of this autore
	 */
	@Override
	public void setUuid(String uuid) {
		_autore.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel
		<it.cloudtec.biblioteca.model.Autore> toCacheModel() {

		return _autore.toCacheModel();
	}

	@Override
	public it.cloudtec.biblioteca.model.Autore toEscapedModel() {
		return new AutoreWrapper(_autore.toEscapedModel());
	}

	@Override
	public String toString() {
		return _autore.toString();
	}

	@Override
	public it.cloudtec.biblioteca.model.Autore toUnescapedModel() {
		return new AutoreWrapper(_autore.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _autore.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AutoreWrapper)) {
			return false;
		}

		AutoreWrapper autoreWrapper = (AutoreWrapper)obj;

		if (Objects.equals(_autore, autoreWrapper._autore)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _autore.getStagedModelType();
	}

	@Override
	public Autore getWrappedModel() {
		return _autore;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _autore.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _autore.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_autore.resetOriginalValues();
	}

	private final Autore _autore;

}