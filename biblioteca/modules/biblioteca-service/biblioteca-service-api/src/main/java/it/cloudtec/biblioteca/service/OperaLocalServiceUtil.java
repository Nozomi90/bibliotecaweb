/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.cloudtec.biblioteca.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Opera. This utility wraps
 * <code>it.cloudtec.biblioteca.service.impl.OperaLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see OperaLocalService
 * @generated
 */
@ProviderType
public class OperaLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.cloudtec.biblioteca.service.impl.OperaLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the opera to the database. Also notifies the appropriate model listeners.
	 *
	 * @param opera the opera
	 * @return the opera that was added
	 */
	public static it.cloudtec.biblioteca.model.Opera addOpera(
		it.cloudtec.biblioteca.model.Opera opera) {

		return getService().addOpera(opera);
	}

	/**
	 * Creates a new opera with the primary key. Does not add the opera to the database.
	 *
	 * @param operaId the primary key for the new opera
	 * @return the new opera
	 */
	public static it.cloudtec.biblioteca.model.Opera createOpera(long operaId) {
		return getService().createOpera(operaId);
	}

	/**
	 * Deletes the opera with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param operaId the primary key of the opera
	 * @return the opera that was removed
	 * @throws PortalException if a opera with the primary key could not be found
	 */
	public static it.cloudtec.biblioteca.model.Opera deleteOpera(long operaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteOpera(operaId);
	}

	/**
	 * Deletes the opera from the database. Also notifies the appropriate model listeners.
	 *
	 * @param opera the opera
	 * @return the opera that was removed
	 */
	public static it.cloudtec.biblioteca.model.Opera deleteOpera(
		it.cloudtec.biblioteca.model.Opera opera) {

		return getService().deleteOpera(opera);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.cloudtec.biblioteca.model.Opera fetchOpera(long operaId) {
		return getService().fetchOpera(operaId);
	}

	/**
	 * Returns the opera matching the UUID and group.
	 *
	 * @param uuid the opera's UUID
	 * @param groupId the primary key of the group
	 * @return the matching opera, or <code>null</code> if a matching opera could not be found
	 */
	public static it.cloudtec.biblioteca.model.Opera fetchOperaByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchOperaByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the opera with the primary key.
	 *
	 * @param operaId the primary key of the opera
	 * @return the opera
	 * @throws PortalException if a opera with the primary key could not be found
	 */
	public static it.cloudtec.biblioteca.model.Opera getOpera(long operaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getOpera(operaId);
	}

	/**
	 * Returns the opera matching the UUID and group.
	 *
	 * @param uuid the opera's UUID
	 * @param groupId the primary key of the group
	 * @return the matching opera
	 * @throws PortalException if a matching opera could not be found
	 */
	public static it.cloudtec.biblioteca.model.Opera getOperaByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getOperaByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the operas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.cloudtec.biblioteca.model.impl.OperaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @return the range of operas
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Opera> getOperas(
		int start, int end) {

		return getService().getOperas(start, end);
	}

	/**
	 * Returns all the operas matching the UUID and company.
	 *
	 * @param uuid the UUID of the operas
	 * @param companyId the primary key of the company
	 * @return the matching operas, or an empty list if no matches were found
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Opera>
		getOperasByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getOperasByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of operas matching the UUID and company.
	 *
	 * @param uuid the UUID of the operas
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of operas
	 * @param end the upper bound of the range of operas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching operas, or an empty list if no matches were found
	 */
	public static java.util.List<it.cloudtec.biblioteca.model.Opera>
		getOperasByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<it.cloudtec.biblioteca.model.Opera> orderByComparator) {

		return getService().getOperasByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of operas.
	 *
	 * @return the number of operas
	 */
	public static int getOperasCount() {
		return getService().getOperasCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the opera in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param opera the opera
	 * @return the opera that was updated
	 */
	public static it.cloudtec.biblioteca.model.Opera updateOpera(
		it.cloudtec.biblioteca.model.Opera opera) {

		return getService().updateOpera(opera);
	}

	public static OperaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<OperaLocalService, OperaLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(OperaLocalService.class);

		ServiceTracker<OperaLocalService, OperaLocalService> serviceTracker =
			new ServiceTracker<OperaLocalService, OperaLocalService>(
				bundle.getBundleContext(), OperaLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}